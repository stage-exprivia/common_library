# Changelog
## [1.4.0] - 06/06/2022
### Added
- CheckMethods(isNumeric, isLocalDate)
- Derived Search Employee Repository
- globalSearch Service Method
- globalSearch Controller Method

## [1.3.0] - 06/06/2022
### Added
- BatchImportInterface
- BatchExportInterface
### Fixed
- Added pagination and filter on batch export

## [1.2.3] - 13/05/2022
### Added
- Controller Query Method exceptions

### Fixed
- Removed @Valid from controller methods signatures
- Fixed return on a method in ResourceNotValidException 
- Removed unnecessary get method in BatchReport


## [1.2.2] - 10/05/2022
### Added
- Exception ResourceNotValidException
- Handled ResourceNotValidException
- EnumValueListType
### Fixed
- Service Exception
- Controller Exception

## [1.2.1] - 06/05/2022
### Fixed
- Pom

## [1.2.0] - 06/05/2022
### Added
- GenericBatchConfig
- Service Batch
- Controller Batch

## [1.0.2] - 06/05/2022
### Fixed
- GIT


## [1.0.1] - 06/05/2022

### Fixed
- Optional<ValueList> to findById
- Not null, not blank to Id employee, project, relation, valuelist with message

## [1.0.0] - 03/05/2022
### Added
- ValueList Model
- ValueList Service Interface
- ValueList Controller Interface
- ValueList Repository
- Convert utility
- Batch Report
- Report
- Generic Filter
- QueryBuilder
- Jackson Library Dependency
- Exception
- GlobalExceptionHandler
- Models(Employee, Project, Relation)
- BatchControllerInterface, ControllerInterface
- Repositories(EmployeeRepository, ProjectRepository, RelationRepository)
- BatchServiceInterface, ServiceInterface
### Changed
- Controller Query from T to List<T>
- Controller Delete from void to ResponseEntity<T>
- Service Delete from void to T

### Fixed
- @ID Annotation on models
- New exception on service, controller