# Common Topology Structure
This repository hosts a common library aimed to collect some base classes for being used in owned microservices

## Create local jar

In order to import this common library you need to install locally first:
  
    mvn clean package
    mvn install:install-file -Dfile="target/common-lib-1.4.0.jar" -DgroupId="com.exprivia" -DartifactId="common-lib" -Dversion="1.4.0" -Dpackaging=jar

In order to change the library version just update the version part of POM file

## Before pushing to master and creating tag

- Please remember to update this file and thus creating the tag.
- Do not forget to add to the CHANGELOG why a new release is required. 
