package com.exprivia.commonlibrary.utils;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class CheckMethods {
	
	public static boolean isNumeric(String string) {
        int intValue;
        System.out.println(String.format("Parsing string: \"%s\"", string));
        if(string == null || string.equals("")) {
            System.out.println("String cannot be parsed, it is null or empty.");
            return false;
        }
        try {
            intValue = Integer.parseInt(string);
        } catch (NumberFormatException e) {
            System.out.println("Input String cannot be parsed to Integer.");
            return false;
        }
        return true;
    }
    
    public static boolean isLocalDate(String string) {
        LocalDate date;
        System.out.println(String.format("Parsing string: \"%s\"", string));
        if(string == null || string.equals("")) {
            System.out.println("String cannot be parsed, it is null or empty.");
            return false;
        }
        try {
            date = LocalDate.parse(string);
        } catch (DateTimeParseException e) {
            System.out.println("Input String cannot be parsed to LocalDate.");
            return false;
        }
        return true;
    }

}
