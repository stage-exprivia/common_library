package com.exprivia.commonlibrary.models;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class Relation {

	@Id
	@NotNull(message = "id must not be null")
	@NotBlank(message = "id must not be blank")
	private String id;
	private Employee emp;
	private Project proj;
	private float allocationPercent;
	private LocalDate startDate;
	private LocalDate endDate;
	private long durationInDays;

}
