package com.exprivia.commonlibrary.models;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.poiji.annotation.ExcelCell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
@Builder
public class ValueList {
	 
	@ExcelCell(0)
    @Id
	@NotNull(message = "id must not be null")
	@NotBlank(message = "id must not be blank")
    private String id;

    @ExcelCell(1)
    @NotNull
    private String value;
    
    @ExcelCell(2)
    @NotNull
    private String type;
    
    @ExcelCell(3)
    private Double cost;
    
    @ExcelCell(4)
    private LocalDate start;
    
    @ExcelCell(5)
    private LocalDate end;

    

}
