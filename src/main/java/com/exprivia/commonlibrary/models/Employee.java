package com.exprivia.commonlibrary.models;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document
public class Employee {
	@Id
	@NotNull(message = "id must not be null")
	@NotBlank(message = "id must not be blank")
	private String cid;
	private String firstName;
	private String lastName;
	private String sex;
	private String resourceType;
	private String status;
	private String departament;
	private String costProfile;
	private String role;
	private String location;
	private LocalDate hiringDate;
    
	
}
