package com.exprivia.commonlibrary.batch;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatchReport <T>{
    private List<Report> report=new ArrayList<>();
    private List<T> validResources=new ArrayList<>();
    private List<T> notValidResources=new ArrayList<>();


    public void addReport(int row,String status, String message) {
        report.add(new Report(row, status, message));
    }

    public void addValidResources(T t) {
        validResources.add(t);
    }

    public void addNotValidResources(T t) {
        notValidResources.add(t);
    }

}