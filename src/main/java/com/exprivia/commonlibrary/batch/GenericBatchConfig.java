package com.exprivia.commonlibrary.batch;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenericBatchConfig {
    public static XSSFWorkbook createWorkbook(String sheetName) {
        XSSFWorkbook workbook = new XSSFWorkbook();

        workbook.createSheet(sheetName);

        CellStyle headerStyle = workbook.createCellStyle();
        CellStyle rowStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        XSSFFont fontStyle = workbook.createFont();
        // styleheader
        font.setBold(true);
        font.setFontHeight(16);
        headerStyle.setFont(font);
        // rowstyle
        fontStyle.setFontHeight(12);
        fontStyle.setBold(false);
        rowStyle.setFont(fontStyle);

        return workbook;
    }

}
