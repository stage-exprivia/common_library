package com.exprivia.commonlibrary.batch;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor @NoArgsConstructor
public class Report {
    private int row;
    private String status;
    private String message;
}

