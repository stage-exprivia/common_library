package com.exprivia.commonlibrary.filter;

import java.lang.reflect.Field;
import java.sql.Date;
import java.time.LocalDate;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

@Component
public class QueryBuilder<T> {

    @Autowired
    GenericFilter<T> genericFilter;

    public Query buildQuery(String filter, Class<T> typeClass) throws IllegalArgumentException, IllegalAccessException {

        T resource = genericFilter.filterParser(filter, typeClass);
        DBObject queryCondition = new BasicDBObject();
        BasicDBList values = new BasicDBList();

        Field[] fields = resource.getClass().getDeclaredFields();
        for(int i=0; i<fields.length ; i++) {
            Field field = fields[i];
            field.setAccessible(true);
            Object value = field.get(resource);
            if(value != null) {
                if(value instanceof LocalDate){
                    Date date = Date.valueOf(value.toString());
                    BasicDBObject query = new BasicDBObject(field.getName(), new BasicDBObject("$gte", date).append("$lt", new Date(System.currentTimeMillis())));
                    values.add(query);
                } else {
                    BasicDBObject query = new BasicDBObject(field.getName(), value);
                    values.add(query);
                }
            }
        }
        queryCondition.put("$and", values);
        return new BasicQuery(queryCondition.toString());
    }
}
