package com.exprivia.commonlibrary.filter;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GenericFilter<T> {

    @Autowired
    ObjectMapper objectMapper;

    public T filterParser(String filter, Class<T> typeClass) {

        Map<String, String> map = new HashMap<>();
        String[] keyVal = filter.split(";");

        for(String x : keyVal) {
            String[] split = x.split(":");
            map.put(split[0], split[1]);
        }

        return objectMapper.convertValue(map, typeClass);
    }
}
