package com.exprivia.commonlibrary.exceptions;

public class NoDataFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6491662807019032653L;

	public NoDataFoundException() {
		super("No data found");
	}

}
