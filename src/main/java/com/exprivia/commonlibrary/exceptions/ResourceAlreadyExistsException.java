package com.exprivia.commonlibrary.exceptions;

public class ResourceAlreadyExistsException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public ResourceAlreadyExistsException(String id) {
        super("Resource with id " + id + " already exists");
       }
}