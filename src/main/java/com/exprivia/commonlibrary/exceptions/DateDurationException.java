package com.exprivia.commonlibrary.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class DateDurationException extends Exception{

	/**
	 * 
	 */

	private static final long serialVersionUID = 7326776972427750668L;


	public DateDurationException() {
		super("Start date must be earlier than end date");
	}
}
