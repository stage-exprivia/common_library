package com.exprivia.commonlibrary.exceptions;

public class ResourceNotFoundException extends Exception {
        private static final long serialVersionUID = 1L;

		public ResourceNotFoundException(String id) {
            super("Resource not found with id " + id + ".");
           }
}
