package com.exprivia.commonlibrary.exceptions;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler (value = ResourceNotFoundException.class)
    public ResponseEntity<Object> resourceNotFoundException (ResourceNotFoundException rnfe){
        return new ResponseEntity<>(rnfe.getMessage(),HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler (value = ResourceAlreadyExistsException.class)
    public ResponseEntity<Object> resourcesAlreadyExistsException (ResourceAlreadyExistsException raee){
        return new ResponseEntity<>(raee.getMessage(),HttpStatus.CONFLICT);
    }
    
    @ExceptionHandler (value = DateDurationException.class)
    public ResponseEntity<Object> dateDurationException (DateDurationException dde){
        return new ResponseEntity<>(dde.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler (value = MethodArgumentNotValidException.class)
    public ResponseEntity<Object> methodArgumentNotValidException (MethodArgumentNotValidException manve){
        List<ObjectError> errorList = manve.getBindingResult().getAllErrors();
        return new ResponseEntity<>(errorList.get(errorList.size()-1).getDefaultMessage(), HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler (value = InvalidExcelFileException.class)
    public ResponseEntity<Object> invalidExcelFileEception (InvalidExcelFileException iefe) {
    	return new ResponseEntity<>(iefe.getMessage(), HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }
    
    @ExceptionHandler (value = NoDataFoundException.class)
    public ResponseEntity<Object> noDataFoundException (NoDataFoundException ndfe) {
    	return new ResponseEntity<>(ndfe.getMessage(), HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler (value = InvalidObjectFieldNameException.class)
    public ResponseEntity<Object> invalidObjectFieldNameException (InvalidObjectFieldNameException iofne) {
    	return new ResponseEntity<>(iofne.getMessage(), HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler(value = ResourceNotValidException.class)
    public ResponseEntity<Object> ResourceNotValidException (ResourceNotValidException ex){
        return new ResponseEntity<>(ex.getValidationError(), HttpStatus.BAD_REQUEST);
    }
   
}
