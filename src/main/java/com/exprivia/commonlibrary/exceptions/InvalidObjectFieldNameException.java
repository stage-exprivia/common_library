package com.exprivia.commonlibrary.exceptions;

public class InvalidObjectFieldNameException extends RuntimeException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidObjectFieldNameException(String msg) {
        super(msg);
    }
}