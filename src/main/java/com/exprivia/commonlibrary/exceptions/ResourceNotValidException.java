package com.exprivia.commonlibrary.exceptions;

import com.exprivia.commonlibrary.batch.ValidationReport;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter @Setter
public class ResourceNotValidException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ValidationReport validationError;
}
