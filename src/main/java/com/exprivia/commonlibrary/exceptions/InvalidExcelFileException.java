package com.exprivia.commonlibrary.exceptions;

public class InvalidExcelFileException extends RuntimeException{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidExcelFileException(String msg) {
        super(msg);
    }
}
