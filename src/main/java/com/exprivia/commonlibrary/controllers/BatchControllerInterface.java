package com.exprivia.commonlibrary.controllers;


import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Size;

import com.exprivia.commonlibrary.exceptions.ResourceNotFoundException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import io.swagger.v3.oas.annotations.Parameter;

public interface BatchControllerInterface<T> {
    public ResponseEntity<T> insertResourcesFromExcel(@Parameter(description = "The file must be of type .xls or .xlsx") @RequestPart("file") MultipartFile file);
    public void exportResourcesIntoExcel(
        @RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
        @RequestParam(name = "pageSize", defaultValue = "10") @Size (max = 100) Integer pageSize,
        @RequestParam(name = "sortField", defaultValue = "cid") String sortField,
        @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir, 
        @RequestParam (name = "queryParams") Optional<String> queryParams,
        HttpServletResponse response) throws IllegalArgumentException, IllegalAccessException, ResourceNotFoundException;
}
