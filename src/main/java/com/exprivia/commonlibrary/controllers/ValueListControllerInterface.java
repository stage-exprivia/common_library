package com.exprivia.commonlibrary.controllers;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.exprivia.commonlibrary.models.ValueList;

public interface ValueListControllerInterface {
	public ResponseEntity<List<ValueList>> getByType(String type);
}
