package com.exprivia.commonlibrary.controllers;


import java.util.List;

import javax.validation.constraints.Size;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.exprivia.commonlibrary.exceptions.DateDurationException;
import com.exprivia.commonlibrary.exceptions.ResourceAlreadyExistsException;
import com.exprivia.commonlibrary.exceptions.ResourceNotFoundException;
import com.exprivia.commonlibrary.exceptions.ResourceNotValidException;
import com.exprivia.commonlibrary.models.Employee;


public interface ControllerInterface <T> {
    public ResponseEntity<T> insertResource(@RequestBody T bodyRequest) throws ResourceAlreadyExistsException, DateDurationException, ResourceNotValidException;
    public ResponseEntity<T> deleteResource(@PathVariable("id") String id) throws ResourceNotFoundException;
    public ResponseEntity<T> getResourceById(@PathVariable("id") String id) throws ResourceNotFoundException;
    public ResponseEntity <List<T>> getAllResources(
        @RequestParam(defaultValue = "") Integer pageNo, 
        @RequestParam(defaultValue = "") @Size (max=25) Integer pageSize,
        @RequestParam(defaultValue = "") String sortBy,
        @RequestParam(defaultValue = "") String sortAscOrDesc);
    public ResponseEntity<List<T>> getQueriedResources(
        @RequestParam(defaultValue = "") Integer pageNo,
        @RequestParam(defaultValue = "") @Size (max = 25) Integer pageSize,
        @RequestParam(defaultValue = "") String sortField,
        @RequestParam(defaultValue = "") String sortDir, 
        @RequestParam () String queryParams) throws IllegalArgumentException, IllegalAccessException, ResourceNotFoundException;
    public ResponseEntity<T> updateResource(@RequestBody T bodyRequest, @PathVariable("id") String id) throws DateDurationException, ResourceNotFoundException, ResourceNotValidException;
    public ResponseEntity<List<T>> globalSearch(String element);
}
