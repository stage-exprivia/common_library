package com.exprivia.commonlibrary.enums;

public enum EnumValueListType {
    TIPO_RISORSA, 
    STATO,
    DIPARTIMENTO,
    RUOLO,
    SEDE,
    PROFILO_DI_COSTO
}
