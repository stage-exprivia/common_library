package com.exprivia.commonlibrary.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.exprivia.commonlibrary.models.Employee;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {

	List<Employee> findByCidOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrSexContainingIgnoreCaseOrDepartamentContainingIgnoreCaseOrResourceTypeContainingIgnoreCaseOrLocationContainingIgnoreCaseOrRoleContainingIgnoreCaseOrCostProfileContainingIgnoreCaseOrStatusContainingIgnoreCase(
			String element, String element2, String element3, String element4, String element5, String element6,
			String element7, String element8, String element9, String element10);

	List<Employee> findByCidOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrSexContainingIgnoreCaseOrDepartamentContainingIgnoreCaseOrResourceTypeContainingIgnoreCaseOrLocationContainingIgnoreCaseOrRoleContainingIgnoreCaseOrCostProfileContainingIgnoreCaseOrStatusContainingIgnoreCaseOrHiringDate(
			String element, String element2, String element3, String element4, String element5, String element6,
			String element7, String element8, String element9, String element10, LocalDate transform);

	List<Employee> findByCidOrFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrSexContainingIgnoreCaseOrDepartamentContainingIgnoreCaseOrResourceTypeContainingIgnoreCaseOrLocationContainingIgnoreCaseOrRoleContainingIgnoreCaseOrCostProfileContainingIgnoreCaseOrStatusContainingIgnoreCaseOrHiringDateBetween(
			String element, String element2, String element3, String element4, String element5, String element6,
			String element7, String element8, String element9, String element10, LocalDate startDate,
			LocalDate endDate);



}