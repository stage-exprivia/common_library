package com.exprivia.commonlibrary.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.exprivia.commonlibrary.models.Relation;

@Repository
public interface RelationRepository extends MongoRepository<Relation, String> {

}