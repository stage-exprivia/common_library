package com.exprivia.commonlibrary.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.exprivia.commonlibrary.models.ValueList;

@Repository
public interface ValueListRepository  extends MongoRepository <ValueList, Integer> {
    
   public List<ValueList> findByType(String type);

   public Optional<ValueList> findById(String id); 

	
}
