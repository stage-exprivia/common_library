	package com.exprivia.commonlibrary.service;

import java.util.List;

import com.exprivia.commonlibrary.exceptions.DateDurationException;
import com.exprivia.commonlibrary.exceptions.ResourceAlreadyExistsException;
import com.exprivia.commonlibrary.exceptions.ResourceNotFoundException;
import com.exprivia.commonlibrary.exceptions.ResourceNotValidException;
import com.exprivia.commonlibrary.models.Employee;

public interface ServiceInterface<T, K>{
	public T createResource(T object) throws ResourceAlreadyExistsException, DateDurationException, ResourceNotValidException;
	public T updateResource(T object, String id) throws ResourceNotFoundException, DateDurationException, ResourceNotValidException;
	public T getResourceById(String id) throws ResourceNotFoundException;
	public T deleteResource(String id) throws ResourceNotFoundException;
	public List<T> getAllResources(Integer pageNumber, Integer pageSize, String sortBy, String sort);
	public List<T> getQueriedResources(Integer pageNum, Integer pageSize, String sortField, String sortDir, String queryParams) throws ResourceNotFoundException, IllegalArgumentException, IllegalAccessException;
	public List<T> globalSearch(String element);
}

