package com.exprivia.commonlibrary.service;

import java.util.List;

import com.exprivia.commonlibrary.models.ValueList;

public interface ValueListServiceInterface {
	 public List<ValueList> getAllValueListByType(String type);
}
