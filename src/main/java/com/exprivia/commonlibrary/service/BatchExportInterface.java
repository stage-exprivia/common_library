package com.exprivia.commonlibrary.service;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import com.exprivia.commonlibrary.exceptions.NoDataFoundException;
public interface BatchExportInterface <T> {
    public List<T> exportResourcesToExcel(Integer pageNum, Integer pageSize,String sortField,String sortDir, Optional<String> queryParams,HttpServletResponse response) throws IOException, NoDataFoundException, IllegalArgumentException, IllegalAccessException;
}
