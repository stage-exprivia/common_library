package com.exprivia.commonlibrary.service;

import java.io.IOException;


import org.springframework.web.multipart.MultipartFile;
public interface BatchImportInterface <T> {
    public T insertResourcesFromExcel(MultipartFile file) throws IOException;
}
